import { defineConfig } from 'windicss/helpers'
import defaultTheme from 'windicss/defaultTheme'
import formsPlugin from 'windicss/plugin/forms'
import typographyPlugin from 'windicss/plugin/typography'
import plugin from 'windicss/plugin'

export default defineConfig({
  darkMode: 'class',
  safelist: ['border-2', 'border-white', 'border-transparent'],
  theme: {
    fontFamily: {
      sans: ['Open Sans', ...defaultTheme.fontFamily.sans],
    },
    extend: {
      container: {
        center: true,
      },
      screens: {
        sm: '360px',
        md: '600px',
        lg: '900px',
        xl: '1300px',
        '2xl': '1536px',
        '3xl': '1920px',
      },
      fontFamily: {
        quicksand: ['Quicksand'],
        handlee: ['Handlee, cursive'],
      },
    },
  },
  shortcuts: {
    'debug-screens':
      'before:bottom-0 before:left-0 before:fixed before:z-[2147483647] before:px-1 before:text-base before:bg-black before:text-white before:shadow-xl @sm:before:content-["screen:sm"] @md:before:content-["screen:md"] @lg:before:content-["screen:lg"] @xl:before:content-["screen:xl"] @2xl:before:content-["screen:2xl"] <sm:before:content-["screen:none"]',
  },
  plugins: [
    plugin(({ addUtilities }) => {
      const newUtilities = {
        '.word-wraping': {
          'text-align': 'justify',
          '-webkit-hyphens': 'auto',
          '-moz-hyphens': 'auto',
          '-ms-hyphens': 'auto',
          hyphens: 'auto',
        },
        '.word-wrap-break': {
          'word-wrap': 'break-word',
        },
        '.max-content': {
          width: 'max-content',
        },
      }
      addUtilities(newUtilities)
    }),
    formsPlugin,
    typographyPlugin({
      dark: true,
    }),
  ],
})
