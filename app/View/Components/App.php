<?php

namespace App\View\Components;

use Illuminate\View\Component;

class App extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct(
        public ?string $title = null
    ) {
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Closure|\Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        $with_title = false;
        if (null === $this->title) {
            $this->title = config('app.name');
        } else {
            $with_title = true;
        }

        return view('components.app', compact('with_title'));
    }
}
