<?php

namespace App\Providers;

use App\Support\LaravelViteManifest;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register()
    {
        $this->app->singleton(
            'laravel-vite-manifest',
            fn () => new LaravelViteManifest()
        );
    }

    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
        Blade::directive(
            'vite',
            fn ($expression) => '{!! App\Facades\ViteManifest::embed('.$expression.') !!}'
        );

        View::addNamespace('blade', resource_path('blade'));
        View::addNamespace('inertia', resource_path('inertia'));
    }
}
