<?php

namespace App\Http\Controllers;

use Auth;
use Inertia\Inertia;

class ExampleController extends Controller
{
    public function views()
    {
        return view('views.pages.index');
    }

    public function blade()
    {
        return view('blade::pages.index');
    }

    public function inertia()
    {
        return Inertia::render('Welcome');
    }

    public function inertiaHome()
    {
        $user = Auth::user();

        return Inertia::render('Home', [
            'user' => $user,
        ]);
    }

    public function inertiaAbout()
    {
        return Inertia::render('About');
    }

    public function inertiaLogin()
    {
        return Inertia::render('Login');
    }
}
