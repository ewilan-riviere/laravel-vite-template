<?php

use App\Http\Controllers\ExampleController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', fn () => view('index'));
Route::get('/', [ExampleController::class, 'views'])->name('home');
Route::get('/blade', [ExampleController::class, 'blade'])->name('blade');
Route::get('/inertia', [ExampleController::class, 'inertia'])->name('inertia.welcome');
Route::get('/inertia/about', [ExampleController::class, 'inertiaAbout'])->name('inertia.about');
Route::get('/inertia/login', [ExampleController::class, 'inertiaLogin'])->name('inertia.login');

Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('/inertia/home', [ExampleController::class, 'inertiaHome'])->name('inertia.home');
});
