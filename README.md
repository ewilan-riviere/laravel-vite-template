# Laravel Vite

## Install Vite

### Laravel Vite Manifest

#### Config

In `config/vite.php`

```php
<?php

return [
  'dev_server' => env('VITE_DEV_SERVER', 'local' === env('APP_ENV')),
  'dev_server_host' => env('VITE_DEV_SERVER_HOST', '127.0.0.1'),
];
```

#### ViteManifest

In `app/Facades/ViteManifest.php`

```php
<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class ViteManifest extends Facade
{
  /**
   * Get the registered name of the component.
   *
   * @return string
   */
  protected static function getFacadeAccessor()
  {
    return 'laravel-vite-manifest';
  }
}
```

#### LaravelViteManifest

In `app/Support/LaravelViteManifest.php`

```php
<?php

namespace App\Support;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;

class LaravelViteManifest
{
  private $manifestCache = [];

  public function embed(?string $name = 'views', ?string $entry = 'app.ts', ?int $port = 3000): string
  {
    if (Config::get('vite.dev_server')) {
      $host = Config::get('vite.dev_server_host');

      return $this->jsImports(
        "http://{$host}:{$port}/{$entry}"
      );
    }

    if ($assets = $this->productionAssets($name, $entry)) {
      return $this->jsImports($assets)
      .$this->jsPreloadImports($name, $entry)
      .$this->cssImports($name, $entry);
    }

    return '';
  }

  private function getManifest(string $name): array
  {
    if (! empty($this->manifestCache[$name])) {
      return $this->manifestCache[$name];
    }

    $manifest = public_path("assets/dist/{$name}/manifest.json");

    if (File::exists($manifest)) {
      $this->manifestCache[$name] = json_decode(File::get($manifest), true);
    }

    return $this->manifestCache[$name] ?? [];
  }

  private function jsImports(string $url): string
  {
    return "<script type=\"module\" crossorigin src=\"{$url}\"></script>";
  }

  private function jsPreloadImports(string $name, string $entry): string
  {
    $res = '';
    foreach ($this->preloadUrls($name, $entry) as $url) {
      $res .= "<link rel=\"modulepreload\" href=\"{$url}\">";
    }

    return $res;
  }

  private function preloadUrls(string $name, string $entry): array
  {
    $urls = [];
    $manifest = $this->getManifest($name);

    if (! empty($manifest[$entry]['imports'])) {
      foreach ($manifest[$entry]['imports'] as $imports) {
        $urls[] = asset("assets/dist/{$name}/".$manifest[$imports]['file']);
      }
    }

    return $urls;
  }

  private function cssImports(string $name, string $entry): string
  {
    $tags = '';
    foreach ($this->cssUrls($name, $entry) as $url) {
      $tags .= "<link rel=\"stylesheet\" href=\"{$url}\">";
    }

    return $tags;
  }

  private function cssUrls(string $name, string $entry): array
  {
    $urls = [];
    $manifest = $this->getManifest($name);

    if (! empty($manifest[$entry]['css'])) {
      foreach ($manifest[$entry]['css'] as $file) {
        $urls[] = asset("assets/dist/{$name}/{$file}");
      }
    }

    return $urls;
  }

  private function productionAssets(string $name, string $entry): string
  {
    $manifest = $this->getManifest($name);

    if (! isset($manifest[$entry])) {
      return '';
    }

    return asset("assets/dist/{$name}/".$manifest[$entry]['file']);
  }
}
```

### Load Laravel Vite Facade

In `app/Providers/AppServiceProvider.php`

```php
<?php

namespace App\Providers;

use App\Support\LaravelViteManifest;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
  /**
   * Register any application services.
   */
  public function register()
  {
    $this->app->singleton('laravel-vite-manifest', function () {
      return new LaravelViteManifest();
    });
  }

  /**
   * Bootstrap any application services.
   */
  public function boot()
  {
    Blade::directive('vite', function ($expression) {
      return '{!! App\Facades\ViteManifest::embed('.$expression.') !!}';
    });

    View::addNamespace('front', resource_path('front'));
  }
}
```

## Packages

- `artesaos/seotools`
  - <https://github.com/artesaos/seotools>
  - `php artisan vendor:publish --provider="Artesaos\SEOTools\Providers\SEOToolsServiceProvider"`
- `barryvdh/laravel-ide-helper`
  - <https://github.com/barryvdh/laravel-ide-helper>
  - `php artisan vendor:publish --provider="Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider" --tag=config`
  - `composer helper`
- `laravel-dump-server`
  - <https://github.com/beyondcode/laravel-dump-server>
  - `php artisan vendor:publish --provider="BeyondCode\DumpServer\DumpServerServiceProvider"`
- `pestphp/pest`
  - <https://github.com/pestphp/pest>
  - `composer test`
- `nunomaduro/larastan`
  - <https://github.com/nunomaduro/larastan>
  - `composer analyse`
- `genl/matice`
  - <https://github.com/GENL/matice>
  - `php artisan vendor:publish --provider="Genl\Matice\MaticeServiceProvider"`
- `tighten/ziggy`
  - <https://github.com/tighten/ziggy>

## Built with some help

- [adr1enbe4udou1n/laravel-rad-stack](https://github.com/adr1enbe4udou1n/laravel-rad-stack)
- [Using Vite with Inertia — Laravel, Vue & Tailwind](https://dev.to/kodeas/using-vite-with-inertia-laravel-vue-tailwind-2h5k)
- [Building a Vue3 Typescript Environment with Vite](https://miyauchi.dev/posts/vite-vue3-typescript/)
- [ChaDonSom/laravel-vite-template](https://github.com/ChaDonSom/laravel-vite-template)
- [Set up Inertia, Vite, Vue3, TypeScript / Build our first Page](https://tech.codivores.com/ltivt-2-inertia-vite-vue3-typescript)
- [Vite with Laravel](https://sebastiandedeyne.com/vite-with-laravel/)
