import { defineStore } from 'pinia'

export const useStore = defineStore('main', {
  state: () => ({
    sidebar: false,
  }),
  actions: {
    toggleSidebar() {
      this.$patch({
        sidebar: !this.sidebar,
      })
    },
    closeSidebar() {
      this.$patch({
        sidebar: false,
      })
    },
  },
})
