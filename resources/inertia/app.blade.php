<x-app :title="config('app.name')">
    <x-slot:head>
        @routes
        @translations
        @inertiaHead
        @vite('admin', 'app.ts', 3300)
    </x-slot:head>

    @inertia
</x-app>
