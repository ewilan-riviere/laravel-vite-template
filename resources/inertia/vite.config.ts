import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { baseConfig, windicssPlugin } from '../../vite.config'
import path from 'path'
import components from 'unplugin-vue-components/vite'
import svgLoader from 'vite-svg-loader'

// https://vitejs.dev/config/
export default defineConfig({
  ...baseConfig(path.basename(__dirname)),
  resolve: {
    alias: {
      '~/inertia': `${__dirname}`,
    },
  },
  plugins: [
    vue(),
    components({
      dirs: ['components', 'layouts'],
      dts: true,
      directoryAsNamespace: true,
    }),
    windicssPlugin(),
    svgLoader(),
  ],
  optimizeDeps: {
    entries: ['inertia/app.ts'],
    include: [
      'vue',
      '@inertiajs/inertia',
      '@inertiajs/inertia-vue3',
      '@inertiajs/progress',
    ],
  },
})
