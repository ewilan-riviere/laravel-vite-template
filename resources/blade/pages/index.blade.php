<x-app>
    <x-slot:head>
        @vite('blade', 'app.ts', 3200)
    </x-slot:head>

    <h1>
        front views
    </h1>
    <div x-data>
        <h1>
            Welcome to the
            <span x-text="$store.shop.name">shop-name</span>
        </h1>
        <div>
            Here you can buy:
            <ul>
                <template x-for="product in $store.shop.products">
                    <li x-text="product"></li>
                </template>
            </ul>
        </div>
    </div>
</x-app>
