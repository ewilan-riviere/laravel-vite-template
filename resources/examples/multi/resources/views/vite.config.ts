import { defineConfig, PluginOption } from 'vite'
import baseConfig from '../vite.config'
import windicss from 'vite-plugin-windicss'

/**
 * Enable full reload for blade file
 */
const laravel = (): PluginOption => ({
  name: 'vite:laravel',
  handleHotUpdate({ file, server }) {
    if (file.endsWith('.blade.php')) {
      server.ws.send({
        type: 'full-reload',
        path: '*',
      })
    }
  },
})

// https://vitejs.dev/config/
export default defineConfig({
  ...baseConfig('views'),
  cacheDir: '../../node_modules/.vite/views',
  resolve: {
    alias: {
      '@views': __dirname,
    },
  },
  plugins: [
    laravel(),
    windicss({
      config: '../../windi.config.ts',
      scan: {
        dirs: ['.', '../views', '../components'],
        fileExtensions: ['blade.php', 'vue', 'ts'],
      },
    }),
  ],
  optimizeDeps: {
    include: ['alpinejs'],
  },
})
