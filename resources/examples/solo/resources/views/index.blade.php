<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    @vite('views', 'app.ts', 3000)
</head>

<body x-data class="dark:bg-gray-900 dark:text-white"
    class="antialiased min-h-screen {{ config('app.env') === 'local' ? 'debug-screens' : '' }}">
    <div class="text-lg">hello</div>
</body>

</html>
