import { defineConfig, PluginOption, UserConfigExport } from 'vite'
import Dotenv from 'dotenv'
import windicss from 'vite-plugin-windicss'

Dotenv.config()

/**
 * Enable full reload for blade file
 */
const laravel = (): PluginOption => ({
  name: 'vite:laravel',
  handleHotUpdate({ file, server }) {
    if (file.endsWith('.blade.php')) {
      server.ws.send({
        type: 'full-reload',
        path: '*',
      })
    }
  },
})

// https://vitejs.dev/config/
export default defineConfig({
  server: {
    hmr: {
      host: process.env.VITE_DEV_SERVER_HOST,
    },
  },
  // base: `${process.env.ASSET_URL || ''}/dist/`,
  base: '',
  root: `resources/views`,
  publicDir: `static`,
  resolve: {
    alias: {
      '~': './',
      '@': './',
    },
  },
  build: {
    outDir: `../../public/assets/dist/`,
    emptyOutDir: true,
    manifest: true,
    rollupOptions: {
      input: './resources/views/app.ts',
    },
  },
  cacheDir: '../../node_modules/.vite',
  plugins: [
    laravel(),
    windicss({
      config: './windi.config.ts',
      scan: {
        dirs: ['./resources/'],
        fileExtensions: ['blade.php', 'vue', 'ts'],
      },
    }),
  ],
  optimizeDeps: {
    include: ['alpinejs'],
  },
})
