import tailwindcss from 'tailwindcss'

export default tailwindcss({
  purge: ['./resources/**/*.{js,jsx,ts,tsx,vue,blade.php}'],
  theme: {
    extend: {},
  },
  plugins: [],
})
