import { defineConfig, PluginOption, UserConfigExport } from 'vite'
import { resolve } from 'path'
// // import vue from '@vitejs/plugin-vue'

const laravel = (): PluginOption => ({
  name: 'vite:laravel',
  handleHotUpdate({ file, server }) {
    if (file.endsWith('.blade.php')) {
      server.ws.send({
        type: 'full-reload',
        path: '*',
      })
    }
  },
})

// export default defineConfig({
//   resolve: {
//     alias: {
//       // '@': resolve(__dirname, 'src'),
//       '@': './resources/',
//     },
//   },
//   plugins: [
//     // vue()
//   ],
// })

// const { resolve } = require('path');
// import vue from '@vitejs/plugin-vue';

// export default ({ command }) => ({
//   base: command === 'serve' ? '' : '/dist/',
//   publicDir: 'fake_dir_so_nothing_gets_copied',
//   build: {
//     manifest: true,
//     outDir: resolve(__dirname, 'public/dist'),
//     rollupOptions: {
//       input: 'resources/js/app.js',
//     },
//   },

//   plugins: [vue()],

//   resolve: {
//     alias: {
//       '@': resolve('./resources/js'),
//     },
//   },
// });

// export default defineConfig({
export default ({ command }): UserConfigExport => ({
  server: {
    // hmr: {
    //   host: process.env.VITE_DEV_SERVER_HOST,
    // },
    // strictPort: true,
    port: 3000,
    // https: true,
    hmr: {
      host: process.env.VITE_DEV_SERVER_HOST,
    },
  },
  // base: `${process.env.ASSET_URL || ''}/dist/`,
  // root: `resources/${command}`,
  // publicDir: false,
  // base: command === 'build' ? '/dist/' : '',
  base: '',
  publicDir: false,
  // build: {
  //   outDir: `../../public/assets/dist/${command}`,
  //   emptyOutDir: true,
  //   manifest: true,
  //   rollupOptions: {
  //     input: '/resources/app.ts',
  //   },
  // },
  build: {
    manifest: true,
    outDir: 'public/dist',
    // rollupOptions: {
    //   input: {
    //     app: '/resources/app.ts',
    //   },
    // },
    rollupOptions: {
      input: 'resources/ts/app.ts',
    },
  },
  cacheDir: '../../node_modules/.vite/views',
  // base: command === 'serve' ? '' : '/build/',
  // publicDir: 'fake_dir_so_nothing_gets_copied',
  // base: '/dist/',
  // publicDir: 'fake_dir_so_nothing_gets_copied',
  // build: {
  //   manifest: true,
  //   outDir: resolve(__dirname, 'public/dist'),
  //   rollupOptions: {
  //     input: 'resources/ts/app.ts',
  //   },
  // },
  // build: {
  //   manifest: true,
  //   rollupOptions: {
  //     input: 'resources/ts/app.ts',
  //   },
  // },

  plugins: [laravel()],

  resolve: {
    alias: {
      '@': resolve('./resources'),
    },
  },

  optimizeDeps: {
    include: ['alpinejs'],
  },
})

// import Dotenv from 'dotenv'

// Dotenv.config()

// export default (entry: string): UserConfigExport => {
//   base: command === 'build' ? '/dist/' : '',
//   publicDir: false,
//   build: {
//     manifest: true,
//     outDir: "public/dist",
//     rollupOptions: {
//       input: {
//         app: "resources/js/app.ts",
//       },
//     },
//   },
// server: {
//   strictPort: true,
//   port: 3030,
//   // https: true,
//   hmr: {
//     host: "localhost",
//   },
// },
//   plugins: [
//     vue()
//   ],
//   optimizeDeps: {
//     include: [
//       "@inertiajs/inertia",
//       "@inertiajs/inertia-vue3",
//       "axios",
//       "vue"
//     ],
//   },
//   plugins: [
//     {
//       name: 'blade',
//       handleHotUpdate({ file, server }) {
//         if (file.endsWith('.blade.php')) {
//           server.ws.send({
//             type: 'full-reload',
//             path: '*',
//           })
//         }
//       },
//     },
//   ],
// })
